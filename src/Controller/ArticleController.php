<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    #[Route('/article')]
    public function index(): Response
    {
        $articles = $this->manager->getRepository(Article::class)->findBy([], ['id' => 'DESC']);

        return $this->render('article/index.html.twig', [
            'articles' => $articles
        ]);
    }

    #[Route('/article/add', name: "/article/add")]
    public function add(Request $request): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($article);
            $this->manager->flush();

            return $this->redirectToRoute('/');
        }

        return $this->render('article/form.html.twig', [
            'form' => $form->createView(),
            'title' => "Création d'un article",
            'buttonText' => "Créer"
        ]);
    }

    #[Route('article/edit/{id}', name: "article/edit")]
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->manager->flush();

            return $this->redirectToRoute('/');
        }

        return $this->render('article/form.html.twig', [
            'form' => $form->createView(),
            'title' => "Édition d'un article",
            'buttonText' => "Modifier"
        ]);
    }
}
