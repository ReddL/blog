<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: '/')]
    public function index(EntityManagerInterface $manager): Response
    {
        $articles = $manager->getRepository(Article::class)->findBy([], ['id' => 'DESC']);

        return $this->render('/home/index.html.twig', [
            'articles' => $articles,
        ]);
    }
}
