# # Labo Dev Web Blog 🍔

Ce repo est dédié à un projet de développement web visant à créer un Blog de cuisine. 🥑🌮🍔
Le groupe qui a réalisé ce projet est composé de Abeille Paul-Antoine et Bourin Léo.

![](https://cdn.discordapp.com/attachments/766669505543405568/971217440526712842/php-symfony-mysql-sample.png)

# Sommaire 📄

- [#](#)
- [Sommaire 📄](#sommaire-)
- [Le projet](#le-projet)
  - [Les outils](#les-outils)
    - [La BDD MySQL](#la-bdd-mysql)
- [Fonctionnalités](#fonctionnalités)
- [Preview du site](#preview-du-site)
- [Groupe](#groupe)

# Le projet

Ce projet est comme dit dans l'introduction un projet de cours de framework web visant à créer un site sur lequel il est possible de poster ses recettes de cuisines ou juste partager sa passion pour la cuisine.


## Les outils

Pour réaliser ce projet, l'éditeur de code utilisé est Visual Studio Code et le langage utilisé est le php avec le framework Symfony PHP.



---

### La BDD MySQL

Pour stocker nos données telles que les infos de login des utilisateurs et les informations concernant les posts, nous avons utilisé une base de donnée MySQL.

# Fonctionnalités

Le site web présente différentes fonctionnalités telles que:
- la possibilité de créer un utilisateur, 
- la création de posts 
- éditer ses posts


# Preview du site

La preview du site sera publiée lorsque celui-ci sera finalisé.

# Groupe 

Abeille Paul-Antoine | Bourin Léo
